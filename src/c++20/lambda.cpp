// is c++11 actually

/*
  # LAMBDA FUNCTIONS

  A lambda function is enclosed in square brackets with the capturing parameters
  and then, like any other function, it has its normal parameters in
  parentheses. It also has a body, on which we can perform operations.
*/

int main(int argc, char const *argv[])
{
    auto fn = [](int &a) -> int { return a + 29; };

    return 0;
}
