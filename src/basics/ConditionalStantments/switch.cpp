/*
-- SINTAXIS DE UN SWITCH --

switch(expresion){
        caso literal1:
                conjunto de instrucciones;
                break;
        caso literal2:
                conjunto de instrucciones;
                break;
        caso literaln:
                conjunto de instrucciones;
                break;
        default:
                conjunto de instrucciones por defecto;
                break;
}
*/
#include <iostream>
using namespace std;

int main()
{
    int numero;

    cout << "Digite un numero entre 1 y 5: ";
    cin >> numero;

    switch (numero) {
    case 1:
        cout << "El numero es 1";
        break;
    case 2:
        cout << "El numero es 2";
        break;
    case 3:
        cout << "El numero es 3";
        break;
    case 4:
        cout << "El numero es 4";
        break;
    case 5:
        cout << "El numero es 5";
        break;
    default:
        cout << "El numero no entra en el rango exigido";
        break;

        return 0;
    }
}
