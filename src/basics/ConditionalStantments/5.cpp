/*
Escribir un programa que te indique si la vocal digitada es minuscula
o mayuscula
*/

#include <iostream>
using namespace std;

int main()
{
    char letra;

    cout << "Colocar una letra: ";
    cin >> letra;

    switch (letra) {
    case 'a':
    case 'e':
    case 'i':
    case 'o':
    case 'u':
        cout << "Minuscula";
        break;
    default:
        cout << "Colocaste cualquier otro caracter o tu vocal es mayuscula ;)";
        break;
    }

    return 0;
}
