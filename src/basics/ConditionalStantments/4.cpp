/*
Comprobar si el numero digitado por el usuario es negativo o positivo
*/

#include <iostream>
using namespace std;

int main()
{
    float numero;

    cout << "Digite un numero: ";
    cin >> numero;

    if (numero == 0) {
        cout << "Cero";
    } else if (numero > 0) {
        cout << "Positivo";
    } else if (numero < 0) {
        cout << "Negativo";
    }

    return 0;
}
