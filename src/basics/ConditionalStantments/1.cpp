/*
Escribe un programa que lea dos numeros y que determine cual
de ellos es el mayor
*/

#include <iostream>
using namespace std;

int main()
{
    float n1, n2;

    cout << "Cuales son tus dos numeros(?): " << endl;
    cin >> n1 >> n2;

    if (n1 == n2) {
        cout << "Tus numeros son iguales";
    } else if (n1 > n2) {
        cout << "Tu primer numero es el mayor";
    } else {
        cout << "Tu segundo numero es el mayor";
    }

    return 0;
}
