/*
Indicar si el caracter digitado por el usuario es una vocal minus o
mayus o si no es vocal
*/

#include <iostream>
using namespace std;

int main()
{
    char letra;

    cout << "Colocar una letra: ";
    cin >> letra;

    switch (letra) {
    case 'a':
    case 'e':
    case 'i':
    case 'o':
    case 'u':
        cout << "Minuscula";
        break;
    case 'A':
    case 'E':
    case 'I':
    case 'O':
    case 'U':
        cout << "Mayuscula";
        break;
    default:
        cout << "No se que pusiste pero no es una vocal";
        break;
    }

    return 0;
}
