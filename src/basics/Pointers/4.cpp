/**
 * @file ejercicio4.cc
 * @author sssimxn (offsivi@gmail.com)
 *
 * @details Relleanr un arrat de 10 numeros, posteriormente utilizando punteros
 *          indicar cuales son numeros pares y su posicion de memoria
 */

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int numeros[10], *dir_num;

    for (int i = 0; i < 10; i++) {
        cout << "Digite un numero [" << i << "]: ";
        cin >> numeros[i];
    }

    dir_num = numeros;

    for (int i = 0; i < 10; i++) {
        if (*dir_num % 2 == 0) {
            cout << "El numero " << *dir_num << " es par" << endl;
            cout << "Posicion: " << dir_num << "\n\n";
        }
        dir_num++;
    }

    getch();
    return 0;
}
