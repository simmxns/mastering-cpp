/* Punteros - Declaración de Punteros
 * &n = la direccion de n
 * n = La variable cuya direccion esta almacenada en n
 *
 * Una variable que almacena la direccion de memoria
 */

#include <iostream>
using namespace std;

int main()
{
    int num, *dir_num;

    num = 20;
    dir_num = &num;

    cout << "Numero: " << num << endl;
    cout << "Direccion de mem: " << &num << endl;
    printf("\n");
    cout << "Numero: " << *dir_num << endl;
    cout << "Direccion de mem: " << &dir_num << endl;

    system("pause");
    return 0;
}