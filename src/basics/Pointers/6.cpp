/*
Asignacion dinamica de arreglos

new : Reserva el numero de bytes solicitado por la declaracion.
delete: Libera un bloque de bytes reservado con anterioridad.

Ejemplo: Pedir al usuario calificaciones y almacenarlos en un arreglo dinamico
*/

#include "conio.h"
#include "iostream"
#include "stdlib.h" // new y delete
using namespace std;

void pedirNotas();
void mostrarNotas();

int numCalif;
int *calif;

int main()
{
    pedirNotas();
    mostrarNotas();

    delete[] calif; // Liberando el espacio en bytes utilizados con aterioridad

    getch();
    return 0;
}

void pedirNotas()
{
    cout << "Digite el numero de calificaciones: ";
    cin >> numCalif;

    calif = new int[numCalif]; // Crear el arreglo

    for (int i = 0; i < numCalif; i++) {
        cout << "Ingrese una nota: ";
        cin >> calif[i];
    }
}

void mostrarNotas()
{
    cout << "\n\nMostrando notas del Usuario:\n";
    for (int i = 0; i < numCalif; i++) {
        cout << calif[i] << endl;
    }
}