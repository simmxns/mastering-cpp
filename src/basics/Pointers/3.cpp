// Correspondencia entre arrays y punteros

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int numeros[] = {1, 2, 3, 4, 5};
    int *dir_numeros;

    dir_numeros = numeros;

    for (int i = 0; i < 5; i++) {
        // cout<<"Elemento del vectos ["<<i<<"]: "<<*dir_numeros++<<endl; //++
        // aumentera 4 bytes para pueda mostrar los demas valores
        cout << "Posiciones de memoria " << numeros[i]
             << " es: " << dir_numeros++ << endl;
    }

    getch();
    return 0;
}
