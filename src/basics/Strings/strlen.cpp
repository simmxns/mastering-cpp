/*
Longitud de una cadena de caracteres - Funcion strlen()
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char palabra[] = "Hola";
    int longg = 0;

    longg = strlen(palabra);

    cout << "Longitud: " << longg << endl;

    system("pause");
    return 0;
}
