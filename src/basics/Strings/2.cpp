/*
Pedir al usuario una cadena de caracteres, almacenarla en
un arreglo y copiar todo su contenido hacia otro
arrelo de caracteres
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char cadena[100];
    char almacen[100];

    cout << "Introduce una cadena: ";
    gets(cadena);

    strcpy(almacen, cadena);
    cout << almacen << endl;

    system("pause");
    return 0;
}
