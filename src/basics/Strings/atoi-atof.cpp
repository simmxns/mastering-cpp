/*
Transformar una cadena a numeros - Funcion atoi() y atof()
"123" -> 123 atoi()
"123.45" -> 123.45 atof()
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int main()
{
    char cad[] = "123";
    char cad2[] = "123.456";
    float fl;
    int num;

    num = atoi(cad);
    cout << num << endl;

    fl = atof(cad2);
    cout << fl << endl;

    system("pause");
    return 0;
}
