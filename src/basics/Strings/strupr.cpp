/*
Pasar palabra a MAYUSCULAS - Funcion strupr()
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cad[] = "siMon";

    strupr(cad); // SIMON
    cout << cad << endl;

    system("pause");
    return 0;
}
