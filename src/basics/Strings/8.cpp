/*
Pedir su nombre al usuario en MAYUS si su nomre comienza
por la letra A, convertir su nombre a minusculas, caso contrario
no convertirlo
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char nombre[100];
    char B[] = "B";
    cout << "Ingrese su nombre en MAYUS: ";
    gets(nombre);

    if (strcmp(nombre, B) < 0) {
        strlwr(nombre);
        cout << nombre << endl;
    } else {
        cout << "No empieza por 'A'" << endl;
    }

    system("pause");
    return 0;
}
