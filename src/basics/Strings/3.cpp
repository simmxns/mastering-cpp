/*
Pedir al usuario que ingrese una cade de caracteres y guardarla en un arreglo y
copiar su contenido a otro arreglo
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cadena[100];
    char otro[100];

    cout << "Enter the string: ";
    gets(cadena);
    strcpy(otro, cadena);
    cout << otro << endl;

    system("pause");
    return 0;
}
