/*
Realice un programa que lea una cadena de caracteres
y muestre en la salida cuantas ocurrencias de cada vocal
en la cadena
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char frase[30];
    int voa = 0, voe = 0, voi = 0, voo = 0, vou = 0;

    cout << "Digite una frase: ";
    cin.getline(frase, 30, '\n');

    for (int i = 0; i < 30; i++) {
        switch (frase[i]) {
        case 'a':
            voa++;
            break;
        case 'e':
            voe++;
            break;
        case 'i':
            voi++;
            break;
        case 'o':
            voo++;
            break;
        case 'u':
            vou++;
            break;
        }
    }

    cout << "Vocal a: " << voa << endl;
    cout << "Vocal e: " << voe << endl;
    cout << "Vocal i: " << voi << endl;
    cout << "Vocal o: " << voo << endl;
    cout << "Vocal u: " << vou << endl;
    cout << "\n";
    cout << "Total de vocales encontradas: " << voa + voe + voi + voo + vou
         << "\n\n";

    system("pause");
    return 0;
}
