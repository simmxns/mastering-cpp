/*
Pedir al usuario que digite 2 cadenas de caracteres, e
indicar si ambas cadenas son iguales, en caso de no serlo,
indicar cual es mayor alfabeticamente
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char palabra1[100];
    char palabra2[100];

    cout << "Digite su primer cadena: ";
    gets(palabra1);
    cout << "Digite su segunda cadena: ";
    gets(palabra2);
    cout << endl;

    if (strcmp(palabra1, palabra2) > 0) {
        cout << palabra1 << " es mayor alfabeticamente" << endl;
    } else {
        cout << palabra2 << " es mayor alfabeticamente" << endl;
    }

    system("pause");
    return 0;
}
