/*
Escriba un fragmento de programa que intercambie los valores
de dos variables
*/

#include <iostream>
using namespace std;

int main()
{
    int x, y, aux;
    // escribir valores y guardarlos
    cout << "Valor de x: ";
    cin >> x;
    cout << "Valor de y: ";
    cin >> y;
    // aux guarda el valor de x
    aux = x;
    // x intercambia el valor con y
    x = y;
    // y intercambia el valor con aux
    y = aux;
    // imprimiendo los valores intercambiados
    cout << "\nEl nuevo valor de x es: " << x << endl;
    cout << "El nuevo valor de y es: " << y << endl;

    return 0;
}
