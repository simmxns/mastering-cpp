/*
Escribe un programa que promedie 4 notas de un alumno
*/

#include <iostream>
using namespace std;

int main()
{
    float nota1, nota2, nota3, nota4, promedio = 0;
    // digitar notas y guardarlas
    cout << "Nota 1: ";
    cin >> nota1;
    cout << "Nota 2: ";
    cin >> nota2;
    cout << "Nota 3: ";
    cin >> nota3;
    cout << "Nota 4: ";
    cin >> nota4;
    // promediar
    promedio = (nota1 + nota2 + nota3 + nota4) / 4;
    // precisar promedio
    cout.precision(3);
    // imprimir promedio
    cout << "\nPromedio: " << promedio << endl;

    return 0;
}
