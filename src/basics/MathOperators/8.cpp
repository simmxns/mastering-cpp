/*
Realice un programa que calcule el valor que toma la siguiente funcion para unos
valores dados de x e y:
*/

#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    float x, y, resultado = 0;

    cout << "Valor de x: ";
    cin >> x;
    cout << "Valor de y: ";
    cin >> y;
    // sqrt "raiz cuadrada" pow "potencia de un numero"
    resultado = (sqrt(x)) / (pow(y, 2) - 1);

    cout << "\nResultado: " << resultado << endl;

    return 0;
}
