/*
Escribe la siguiente expresion matematica como exprecion C++
a + (b / c - d)
*/
#include <iostream>
using namespace std;

int main()
{
    // definiendo valores
    float a, b, c, d, resultado = 0;
    // colocar valores y guardarlos
    cout << "valor de a: ";
    cin >> a;
    cout << "valor de b: ";
    cin >> b;
    cout << "valor de c: ";
    cin >> c;
    cout << "valor de d: ";
    cin >> d;
    // operacion matematica
    resultado = a + (b / c - d);
    // precisar resultado
    cout.precision(3);
    // imprimir resultado
    cout << "\nResultado: " << resultado << endl;

    return 0;
}
