/*
Escriba una funcion llamada mult() que acepte dos numeros en punto flotante como
parametros, multiplique estos dos numeros y despliegue el resultado

despliegue = void
retorne = int - float - ...
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

void pedirDatos();
void mult(float x, float y);

float num1, num2;

int main()
{
    pedirDatos();
    mult(num1, num2);

    system("pause"); // En la terminal de linux (bash) arroja 'sh: 1: pause: not
                     // found' en otros compiladores (dev c++) evita que se
                     // cierre el programa
    return 0;
}

void pedirDatos()
{
    cout << "Digite 2 numeros: ";
    cin >> num1 >> num2;
}
void mult(float x, float y)
{
    float multi = x * y;

    cout << "Multiplicacion: " << multi << endl;
}