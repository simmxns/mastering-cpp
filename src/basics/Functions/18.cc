/* Realiza una funcion que tome como parametros un vector de enteros
y su tamaño e imprima un vector con los elementos impares del vector
recibido */

#include <iostream>
using namespace std;

void pedirDatos();
void imprimirImpares(int[], int);

int vec[100], tam;

int main(int argc, const char **argv)
{

    pedirDatos();
    imprimirImpares(vec, tam);

    cin.ignore();
    cin.get();
    return 0;
}

void pedirDatos()
{
    cout << "Rellenar vector y tamaño del vector\n";
    cout << "Tamaño del vector: ";
    cin >> tam;

    for (int i = 0; i < tam; i++) {
        cout << i + 1 << ") Ingresar un numero: ";
        cin >> vec[i];
    }
}

void imprimirImpares(int vec[], int tam)
{
    int vecimp[100];
    int j = 0;

    for (int i = 0; i < tam; i++) {
        if (vec[i] % 2 != 0) {
            vecimp[j] = vec[i];
            j++;
        }
    }

    cout << "\nImprimiendo vector de impares...\n";
    for (int i = 0; i < j; i++) {
        cout << vecimp[i] << " ";
    }
}