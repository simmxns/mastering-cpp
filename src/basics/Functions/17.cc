/* Realiza una funcion que tome como parametros un vectro de nuemros
y su tamañp y cambie el signo de los elementos del vector  */

#include <iostream>

using namespace std;

void pedirDatos();
void cambiarSigno(int vec[], int);
void mostrarVec(int vec[], int);

int vec[100], tam;

int main()
{

    pedirDatos();
    cambiarSigno(vec, tam);
    mostrarVec(vec, tam);

    cin.ignore();
    cin.get();
    return 0;
}

void pedirDatos()
{
    cout << "Digite el tamaño del vector: ";
    cin >> tam;

    for (int i = 0; i < tam; i++) {
        cout << i + 1 << ") Digite un numero: ";
        cin >> vec[i];
    }
}

void cambiarSigno(int vec[], int tam)
{

    for (int i = 0; i < tam; i++) {
        vec[i] *= -1; // vec[i] = vec[i] * -1
    }
}

void mostrarVec(int vec[], int tam)
{
    cout << "\nMostrando elementos del vector con signo cambiardo\n";
    for (int i = 0; i < tam; i++) {
        cout << vec[i] << " ";
    }
}