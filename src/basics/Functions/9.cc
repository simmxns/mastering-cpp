/* Intercambiar el valor de 2 variables utilizando paso de parametros
por referencia */

#include <iostream>
using namespace std;

void intercambiar(int &, int &);

int main()
{
    int num1 = 10, num2 = 15;

    cout << "valor de num1: " << num1 << endl;
    cout << "valor de num2: " << num2 << endl;

    intercambiar(num1, num2);

    cout << "\nNuevo valor de num1: " << num1 << endl;
    cout << "Nuevo valor de num2: " << num2 << endl;

    cin.ignore();
    cin.get();
    return 0;
}

void intercambiar(int &num1, int &num2)
{
    int aux;

    aux = num1;
    num1 = num2;
    num2 = aux;
}