/**
 * Escriba una funcion escribeNumeros(int ini,int fin) que devuelva en la salida
 *estandar los numeros del ini del fin. Escriba una version que escriba los
 *numeros en orden ascendente escribeNumeros(ini,fin) = ini        , si ini=fin
 *		     escribeNumeros(ini,fin-1)       , si fin>ini
 */

#include "iostream"
#include "stdlib.h"

using namespace std;

int escribeNumeros(int, int);
// int ordenAsc(int,int);

int main()
{
    int ini, fin;

    cout << "Valor Inicial: ";
    cin >> ini;
    cout << "Valor Final: ";
    cin >> fin;
    cout << endl;

    for (int i = ini; i <= fin; i++) {
        cout << escribeNumeros(i, fin) << " ";
    }
    printf("\n");
    for (int i = fin; i >= ini; i--) {
        cout << escribeNumeros(i, fin) << " ";
    }

    printf("\n");
    system("pause");
    return 0;
}

int escribeNumeros(int ini, int fin)
{
    if (ini == fin) {
        return ini;
    } else {
        return escribeNumeros(ini, fin - 1);
    }
}

/* int ordenAsc(int ini, int fin){
    if(fin==ini){
        return fin;
    } else {
        return escribeNumeros(fin,ini-1);
    }
} */