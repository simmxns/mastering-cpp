/* Realice una funcion que tome como parametros un vector
y su tamaño y diga si el vector esta ordenado crecientemente.
Sugerencia: compruebe que pasa todas las posiciones del vector,
salvo para la 0, el elemeto del vector es mayor o igual al elemento
que le precede en el vector */

#include "iostream"
#include "vector"
using namespace std;

char ordenarVec(int vec[], int);
void llenarVec(int vec[], int);

int vec[100], tam;

int main()
{
    char band;

    llenarVec(vec, tam);

    band = ordenarVec(vec, tam);
    if (band == 'F') {
        cout << "Ordenado crecientemente";
    } else {
        cout << "NO ordenado crecientemente";
    }

    cin.ignore();
    cin.get();
    return 0;
}

char ordenarVec(int vec[], int tam)
{
    char band = 'F';

    // 1 2 3 4 5
    int i = 0;
    while ((band == 'F') && (i <= tam - 1)) {
        if (vec[i] > vec[i + 1]) {
            band = 'V';
        }
        i++;
    }
    return band;
}

void llenarVec(int vec[], int tam)
{
    cout << "Tamaño del arrglo: ";
    cin >> tam;
    for (int i = 0; i < tam; i++) {
        cout << i + 1 << ") Ingresar dato: ";
        cin >> vec[i];
    }
}