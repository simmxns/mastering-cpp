/*Recusividad

Factorial de un numero 3! = 3*2!

    0! = 1

    factorial(n) = 1 , si n=0
    n*factorial(n-1) , si n>0 */

#include <iostream>
#include <stdlib.h>
using namespace std;

int factorial(int);

int main(int argc, char const *argv[])
{

    int n = 3;
    cout << "Factorial de " << n << " = " << factorial(n) << endl;

    cin.ignore();
    cin.get();
    system("pause");
    return 0;
}

int factorial(int n)
{
    if (n == 0) { // Caso base
        n = 1;
    } else { // Caso general
        n = n * factorial(n - 1);
    }

    return n;
}
