/* Paso de parametros de tipo vector

Parametros de la funcion
    void nombreDeFuncion(tipo nombreArreglo[], int tamañoArrelo)

Llamada a la funcion
    nombreDeFuncion(nombreArreglo, tamañoArreglo) */

// Cuadrados de los elementos del vector

#include <iostream>
// #include<conio.h>
using namespace std;

void muestra(int vec[], int);
void cuadrado(int vec[], int);

int main()
{
    const int TAM = 5;
    int vec[TAM] = {1, 2, 3, 4, 5};

    cuadrado(vec, TAM);
    muestra(vec, TAM);

    getchar();
    // cin.ignore();
    // cin.get();
    return 0;
}

void cuadrado(int vec[], int tam)
{
    for (int i = 0; i < tam; i++) {
        vec[i] *= vec[i];
    }
}

void muestra(int vec[], int tam)
{
    for (int i = 0; i < tam; i++) {
        cout << vec[i] << " ";
    }
}