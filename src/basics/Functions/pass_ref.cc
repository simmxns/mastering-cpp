/*
Paso de parametros por Referencia
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

void valNuevo(int &, int &);

int main()
{
    int num1, num2;

    cout << "Digite 2 numeros: ";
    cin >> num1 >> num2;

    valNuevo(num1, num2);

    cout << "Nuevo valor 1er num: " << num1 << endl;
    cout << "Nuevo valor 1do num: " << num2 << endl;

    system("pause");
    return 0;
}

void valNuevo(int &xnum, int &ynum)
{
    cout << "Valor 1: " << xnum << endl;
    cout << "Valor 2: " << ynum << endl;

    xnum = 89;
    ynum = 45;
}
