/* Realice una funcion que dada una matriz y un numero de fila de la
matriz devuelva el menor de los elementos almacenados en dicha fila */

#include <iostream>
using namespace std;

void pedirDatos();
int comprobarDatomenor(int m[][100], int, int);

int m[100][100], nfilas, ncol;

int main(int argc, char const *argv[])
{

    pedirDatos();
    comprobarDatomenor(m, nfilas, ncol);

    cin.ignore();
    cin.get();
    getchar();
    return 0;
}

void pedirDatos()
{
    cout << "Numero de filas: ";
    cin >> nfilas;
    cout << "Numero de columnas: ";
    cin >> ncol;

    cout << "Ingresa los datos de la matriz\n";
    for (int i = 0; i < nfilas; i++) {
        for (int j = 0; j < ncol; j++) {
            cout << "[" << i << "][" << j << "]: ";
            cin >> m[i][j];
        }
    }
}

int comprobarDatomenor(int m[][100], int nfilas, int ncol)
{
    int fila, menor = 99999;

    cout << "\nDigite el numero de fila a comprobar: ";
    cin >> fila;

    for (int i = 0; i < nfilas; i++) {
        for (int j = 0; j < ncol; j++) {
            if (i == fila) {
                if (m[i][j] < menor) {
                    menor = m[i][j];
                }
            }
        }
    }

    return menor;
}