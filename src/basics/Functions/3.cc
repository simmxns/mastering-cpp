/*
Escriba una funcion nombreda funpot() que eleve un entero que
se le transmita una potencia en numero entero positivo y despliegue
el resultado. El numero entero positivo debera ser le segundo valor
transmitido a la funcion
*/

#include <iostream>
#include <math.h>
#include <stdlib.h>
using namespace std;

void pedirDatos();
int funpot(int e, int p);
int e, p;

int main()
{
    int res;
    pedirDatos();
    res = funpot(e, p);
    cout << "Resultado: " << res << endl;

    //    system("pause");
    return 0;
}

void pedirDatos()
{
    cout << "Digite el exponente: ";
    cin >> e;
    cout << "Digite la pontecia: ";
    cin >> p;
}

int funpot(int e, int p)
{
    int res;
    res = pow(e, p);
    return res;
}