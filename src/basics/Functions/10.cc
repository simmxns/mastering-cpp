/* Escriba una funcion nombrada cambio() que tenga un parametro
en numero entero y seis parametros de referencia en numero entero
nombrados cien, cincuenta, veinte, diez, cinco y uno, respectivamente.
La funcion tiene que considerar el valor entero transmitido como cantidad en
dolares y convertir el valor en el numero menor de billetes equivalentes */

#include <iostream>
using namespace std;

void cambio(int, int &, int &, int &, int &, int &, int &);

int main()
{
    int num;
    int cien = 100, cincuenta = 50, veinte = 20, diez = 10, cinco = 5, uno = 1;

    cout << "Digite el valor de num: ";
    cin >> num;

    cambio(num, cien, cincuenta, veinte, diez, cinco, uno);

    cout << "Cien: " << cien << endl;
    cout << "Cincuenta: " << cincuenta << endl;
    cout << "Veinte: " << veinte << endl;
    cout << "Diez: " << diez << endl;
    cout << "Cinco: " << cinco << endl;
    cout << "Uno: " << uno << endl;

    cin.ignore();
    cin.get();
    return 0;
}

void cambio(int num, int &cien, int &cincuenta, int &veinte, int &diez,
    int &cinco, int &uno)
{

    cien = num / 100;
    num %= 100;
    cincuenta = num / 50;
    num %= 50;
    veinte = num / 20;
    num %= 20;
    diez = num / 10;
    num %= 10;
    cinco = num / 5;
    uno = num % 5;
}