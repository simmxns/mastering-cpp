/*
Realizar un programa con una funcion que te encuentre el numero mayor y menor
de 3 numeros
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int minmax(int n1, int n2, int n3);

int main()
{
    int n1 = 10, n2 = 2, n3 = 5;

    minmax(n1, n2, n3);
    //    system("pause");
    return 0;
}

int minmax(int n1, int n2, int n3)
{
    if (n1 < n2 && n1 < n3) {
        cout << n1 << " Menor" << endl;
    } else if (n2 < n1 && n2 < n3) {
        cout << n2 << " Menor" << endl;
    } else {
        cout << n3 << " Menor" << endl;
    }

    if (n1 > n2 && n1 > n3) {
        cout << n1 << " Mayor" << endl;
    } else if (n2 > n1 && n2 > n3) {
        cout << n2 << " Mayor" << endl;
    } else {
        cout << n3 << " Mayor" << endl;
    }
}