/*
Escribir un programa que defina un vector de numeros
y muestre en la salida estandar el vector en orden inverso
del utlimo al primer elemento
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int numero[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    for (int i = 9; i >= 0; i--) {
        cout << numero[i] << endl;
    }

    system("pause");
    return 0;
}
