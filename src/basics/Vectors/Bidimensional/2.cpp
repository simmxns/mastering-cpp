/*
Encontrar la diagonal principal de una matriz de 3x3
*/

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int num[3][3] = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    cout << "Matriz"
         << "\n";
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << num[i][j];
        }
        cout << "\n";
    }
    cout << "Diagonal principal"
         << "\n";
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (i == j) {
                cout << num[i][j];
            }
        }
    }
    cout << endl;
    getch();
    return 0;
}
