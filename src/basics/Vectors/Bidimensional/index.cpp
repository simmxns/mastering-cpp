#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int num[100][100], f, c;
    cout << "Numero de filas: ";
    cin >> f;
    cout << "Numero de columnas: ";
    cin >> c;

    // Almacenando elementos en la matriz
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            cout << "Digite un numero [" << i << "][" << j << "]: "; //[0][0]
            cin >> num[i][j];
        }
    }
    // Mostrar la matriz
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            cout << num[i][j];
        }
        cout << "\n";
    }

    getch();
    return 0;
}
