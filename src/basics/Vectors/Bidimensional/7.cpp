/*
Desarrollar un programa que me indique si una matriz es simétrica o no
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int mat[100][100], f, c;
    char band = 'F';

    cout << "Numero de filas: ";
    cin >> f;
    cout << "Numero de columnas: ";
    cin >> c;

    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            cout << "Digite los numeros de su matriz[" << i << "][" << j
                 << "]: ";
            cin >> mat[i][j];
        }
    }

    if (f == c) {
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                if (mat[i][j] == mat[j][i]) {
                    band = 'V';
                }
            }
        }
    }

    if (band != 'V') {
        cout << "No es simetrica" << endl;
    } else {
        cout << "Simetrica" << endl;
    }

    system("pause");
    return 0;
}
