/*
Realice un programa que calcula la sema de dos matrices cuadradas de 3x3
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int mat1[3][3] = {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
    int mat2[3][3] = {
        {10, 11, 12},
        {13, 14, 15},
        {16, 17, 18}
    };
    int sum1 = 0, sum2 = 0, sumatot = 0;

    for (int f = 0; f < 3; f++) {
        for (int c = 0; c < 3; c++) {
            sum1 += mat1[f][c];
            sum2 += mat2[f][c];
        }
    }

    cout << "Suma de las dos matrices \n";
    sumatot = sum1 + sum2;
    cout << sumatot << endl;

    system("pause");
    return 0;
}
