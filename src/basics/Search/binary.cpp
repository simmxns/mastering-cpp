/*
Busqueda Binaria
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int main()
{
    int num[] = {1, 2, 3, 4, 5}; //*ASC*
    int inf, sup, mitad, dato;
    char band = 'F';

    dato = 4;
    // Algoritmo Busqueda Binaria
    inf = 0;
    sup = 5;

    while (inf <= sup) {
        mitad = (inf + sup) / 2;

        if (num[mitad] == dato) {
            band = 'V';
            break;
        }
        if (num[mitad] > dato) {
            sup = mitad;
            mitad = (inf + sup) / 2;
        }
        if (num[mitad] < dato) {
            inf = mitad;
            mitad = (inf + sup) / 2;
        }
    }

    if (band == 'V') {
        cout << "Encontrado en la posicion: " << mitad << endl;
    } else {
        cout << "No encontrado" << endl;
    }

    system("pause");
    return 0;
}
