/*
Realizar un programa que le pida al usuario digitar cualquier numero
pero si estos numeros no son cero seguir con el programa, en el momento
de digitar cero me muestre el numero mayor hasta que fue colocado el
cero
*/

#include <iostream>
using namespace std;

int main()
{
    float numero, conteo = 0;

    do {
        cout << "Digitar un numero: ";
        cin >> numero;
        if (numero > 0) {
            conteo += 1;
        }
    } while (numero != 0);

    if (conteo == 1) {
        cout << "Colocaste " << conteo << " numero antes de poner cero" << endl;
    } else {
        cout << "Colocaste " << conteo << " numeros antes de poner cero"
             << endl;
    }

    return 0;
}
