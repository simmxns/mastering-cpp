/*
Tabla de multiplicar con un while
*/

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int i, n;
    i = 1;

    cout << "Que tabla quieres ver(?): ";
    cin >> n;

    while (i <= 10) {

        cout << n << " * " << i << " = " << n * i << endl;

        i++;
    }

    getch();
    return 0;
}
