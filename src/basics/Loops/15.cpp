/*
Realizar un programa que pida al usuario pensar un numero
entero entre 1 y 100. El programa debe generar un numero
aleatorio eese mismo rango[1-100], e indicarle al usuario
si el numero que digita es menor o mayor al numero aleatorio,
asi hasta que lo advine. y por ultimo mostrarle el numero
de intentos que le llevo.

variable = limite_inferior + rand() % (limite_superior +1 - limite_inferior);
*/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main()
{
    int numero, dato, contador = 0;

    srand(time(NULL));         // generar un numero aleatorio
    dato = 1 + rand() % (100); // rango del numero aleatorio "[1-100]"

    do {
        cout << "Digite un numero: ";
        cin >> numero;

        if (numero > dato) {
            cout << "\nDigita un numero menor\n";
        }
        if (numero < dato) {
            cout << "\nDigite un numero mayor\n";
        }
        contador++; // cada vez que el bucle se repita "contador" sera sumado
    } while (numero != dato); // si el numero colocado es diferente del numero
                              // aleatorio "dato" el bucle seguir� repitiendose

    cout << "\nFELICIDADES ADIVINASTE EL NUMERO\n";
    cout << "Numero de intentos: " << contador << endl;

    system("pause");
    return 0;
}
