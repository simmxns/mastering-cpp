/*
En un clase de 5 alumnos se han realizado tres examenes y se requiere
determinar el numero de:

a) Alumnos que aprobaron todos los examenes
b) Alumnos que aprobaron al menos un examen.
c) Alumnos que aprobaron unicamente el ultimo examen

Realice un programa que permita la lectura de los datos y el
calculo de las estadisticas
*/

#include <iostream>
using namespace std;

int main()
{
    int n1, n2, n3, i;
    int all = 0, alg = 0, ult = 0;

    for (i = 1; i <= 5; i++) {
        cout << "Notas del alumno " << i << ": ";
        cin >> n1 >> n2 >> n3;

        if (n1 > 5 && n2 > 5 && n3 > 5) {
            all++;
        }
        if (n1 > 5 && n2 > 5 && n3 > 5) {
            alg++;
        }
        if (n1 < 6 && n2 < 6 && n3 > 5) {
            ult++;
        }
    }

    cout << "\nAprobaron todo " << all << " alumnos";
    cout << "\nLos alumnos que aprobaron algunos examenes fueron " << alg;
    cout << "\nLos alumnos que aprobaron SOLO el ultimo examen fueron " << ult;

    return 0;
}
