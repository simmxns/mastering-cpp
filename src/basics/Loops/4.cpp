/*
Realizar un programa que te haga registrar la temperatura cada 4 horas en un
plazo de 24hs (quiere decir que registrara 6 temperaturas) sacar la media de
temperatura, la temp mas baja y la mas alta
*/

#include <iostream>
using namespace std;

int main()
{
    float temp, sumatemp = 0, mediatemp;
    float bajatemp = 999, altatemp = 0;

    for (int i = 0; i < 24; i += 4) {
        cout << "Cual fue la temperatura a las " << i << ": ";
        cin >> temp;

        sumatemp += temp; // sumando las temperaturas y guardandolas en la
                          // variable "sumatemp"

        if (temp > altatemp) { // si "temp" es mayor a "altatemp" se guarda en
                               // "temp" y lo vuelve a preguntar como
                               // repeticiones especifique en el for
            altatemp = temp;
        }
        if (temp < bajatemp) { // si "temp" es menor a "bajatemp" se guarda en
                               // "temp" y lo vuelve a preguntar como
                               // repeticiones especifique en el for
            bajatemp = temp;
        }
    }

    mediatemp = sumatemp / 6;

    cout << "Temperatura media: " << mediatemp << endl;
    cout << "Temperatura baja: " << bajatemp << endl;
    cout << "Temperatura alta: " << altatemp << endl;

    return 0;
}
