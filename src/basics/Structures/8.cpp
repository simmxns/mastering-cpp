/*
Realizar una estructura que contenga los campos personas y un valor logico que
identifique si tiene una discapidad (en caso de no tenerla otro valor logico),
Luego crear dos vectores que contenga en una las personas sin discapacidad y en
el otro las personas con discapacidad
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct personas {
    char nombre[20];
    bool discapacidad;
    //	char cual[30];
} personas[100], personascon[100], personassin[100];

int main()
{
    int n, j = 0, k = 0;

    cout << "Cantidad de personas: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        fflush(stdin);
        cout << "Nombre: ";
        cin.getline(personas[i].nombre, 20, '\n');
        fflush(stdin);
        cout << "Discapacidad(1/0): ";
        cin >> personas[i].discapacidad;

        fflush(stdin);
        if (personas[i].discapacidad ==
            1) { // Si tiene pregunta cual es su discapacidad y lo guarda en
                 // personascon
            //			printf("%cCu%cl es su
            // discapacidad%c",168,160,63);
            // cin.getline(personascon[j].cual,30,'/n');
            strcpy(personascon[j].nombre, personas[i].nombre);
            j++;
        } else { // Si no tiene solo guarda en personassin
            strcpy(personassin[k].nombre, personas[i].nombre);
            k++;
        }
    }
    // Imprimiendo personas con discapacidad
    cout << "Personas con discapacidad";
    for (int i = 0; i < n; i++) {
        cout << personascon[i].nombre << "\n";
        //		cout<<"Discapacidad: "<<personascon[i].cual;
    }
    // Imprimiendo personas sin discapacidad
    cout << "\n\nPersonas sin discapacidad";
    for (int i = 0; i < n; i++) {
        cout << personassin[i].nombre << "\n";
    }
    system("pause");
    return 0;
}
