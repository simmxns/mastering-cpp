/*
Estructura - Es una coleccion de uno o mas tipos de
elementos denominados campos, cada uno de los cuales
puede ser un tipo de dato diferente

SINTAXIS
struct* "nombredelaestructura" {
                <tipodedato> <nombredecampo>
        };
*/

#include <iostream>
#include <stdlib.h>
using namespace std;
// Estructura basica en C++
struct Persona {
    char nombre[20];
    int edad;
} persona1 = {"Simon", 17}, persona2 = {"Andres", 18};

int main()
{

    cout << "Nombre1: " << persona1.nombre << endl;
    cout << "Edad1: " << persona1.edad << endl;

    cout << "\nNombre2: " << persona2.nombre << endl;
    cout << "Edad2: " << persona2.edad << endl;

    system("pause");
    return 0;
}
