/*
Defina una estructura que indique el tiempo empleado
por un ciclista en una etapa. la estructura debe tener tres
campo: horas, minutos y segundos. Escriba un programa que dado n
etapas calcule el tiempo empleado en correr todas la etapas
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct tiempo {
    int hrs;
    int min;
    int seg;
} etps[100];

int main()
{
    int n;
    int hrsT = 0, minT = 0, segT = 0;

    cout << "Cantidad de etapas: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << i + 1;
        printf("%c Etapas\n",
            167); // Caracter especial n�mero 167 de la tabla ASCII "�"
        cout << "Horas: ";
        cin >> etps[i].hrs;
        cout << "Minutos: ";
        cin >> etps[i].min;
        cout << "Segundos: ";
        cin >> etps[i].seg;
        cout << endl;

        hrsT += etps[i].hrs;
        minT += etps[i].min;
        if (minT >= 60) {
            minT -= 60;
            hrsT++;
        }
        segT += etps[i].seg;
        if (segT >= 60) {
            segT -= 60;
            minT++;
        }
    }

    printf("\nEl ciclista tardara\n");
    cout << hrsT << ":" << minT << ":" << segT;
    printf("\nen terminar el circuito\n\n");

    system("pause");
    return 0;
}
