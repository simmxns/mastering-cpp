// #27 thinking that raw pointers are somehow bad
#include <iostream>

std::shared_ptr<int> max(std::shared_ptr<int> a, std::shared_ptr<int> b)
{
  return *a > *b ? a : b;
}

// if ur function doesnt have anything to do with the ownership of objects in question,
// then there's no need to use smart pointers
const int *
max2(const int *a, const int *b)
{
  return *a > *b ? a : b;
}

// the convention is that raw pointers dont own what they're pointing to
// this should not be confused with const-ness of the pointer

// this function is not in charge of the lifetime of either of the pointers 
void add(int *dst, const int *src)
{
  *dst += *src;
} 

// if u r interoperating with c code, c is not going to share this convention
// a c function might very well return some headp-allocated memory to u, that it expects u to delete
// char *someCFunction();
int *someCFunction();

struct FreeDeleter
{
  void operator()(void* x) { free(x); }
};

void doWork()
{
  // be careful, if u got a pointer with malloc (allocates a block of uninitialized memory to a pointer) u need to delete it with free
  // u cant just let unique_pointer try to delete it with the built-in delete
  // u can still unique pointer, but define ur own deleter which use free
  auto data = std::unique_ptr<int, FreeDeleter>(someCFunction());
  // auto data = std::unique_ptr<int>(someCFunction());
  // int *data = someCFunction();
  // char *p = someCFunction();
}
