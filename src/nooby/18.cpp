// #21 thinking that evaluation order is guaranteed to be left to right
#include <iostream>
#include <string>

void funcEvaluationOrderNotGuaranteed()
{
  std::string s = "but i have heard it works even if you don't believe it";

  s.replace(0, 4, "")
    .replace(s.find("even"), 4, "only")
    .replace(s.find("don't"), 6, "");

  // std::string expected = "but i have heard it works even if you don't believe in it";
  // replacing left -> right ??
  std::string expected = "i have heard it works only if you believe in it";
  // but prior of c++ 17, the compiler is actually allowed to compute any sub-expression in any order
  // std::string expected = "i have heard it works evenonlyyou don't believe in it";
  // good news in c++ 17 this example is guaranteed   
}

// however even in c++ 20 the order that function arguments are evaluated is still not guaranteed left to right
int a();
int b();
int c();
int g(int, int, int);

void funcEvaluationOrderNotGuaranteed2()
{
  // this wouldnt matter much if a b and c were pure functions
  // but if a b and c have side effects, then the order that they're called might actually make difference
  g(a(), b(), c());
}