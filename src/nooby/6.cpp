// #7 casting away const
#include <iostream>
#include <unordered_map>
#include <vector>

const std::string &moreFrequent(
    const std::unordered_map<std::string, int> &wordCounts,
    const std::string &word1, const std::string &word2)
{
    // auto &counts = const_cast<std::unordered_map<std::string, int>
    // &>(wordCounts); when compile throw an error msg telling u that the method
    // isnt marked as a const for this you need to add the code of the line 12
    // BAD
    // #8 not knowing map bracket inserts elements
    // Thats right, simply trying to look at this word in the map actually
    // inserts a count of zero into the map return counts[word1] > counts[word2]
    // ? word1 : word2;

    // GOOD
    return wordCounts.at(word1) > wordCounts.at(word2) ? word1 : word2;
}
