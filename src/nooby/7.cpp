// #9 ignoring const-correctness
#include <iostream>
#include <vector>

// Must be const, because the parameter wont be modified, its clear to detect
// now
void printVecOnePerLine(const std::vector<int> &arr)
{
    for (const auto &x : arr) {
        std::cout << x << '\n';
    }
}