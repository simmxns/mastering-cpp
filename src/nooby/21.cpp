// #26  any manual resource management
// this is pretty much the same story as with new and delete
#include <iostream>
#include <fstream>
#include <filesystem>

void readFromAFile(char *name)
{
  // FILE *fp = fopen(name, "r");

  // ... work with the file, EXCEPTION?

  // fclose(fp);

  std::ifstream input{name}; // RAII => Resource Acquisition Is Initialization

  // file WILL be closed
}