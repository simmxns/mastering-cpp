// #5 using C array over standard array
#include <iostream>

// BAD
/*
void fn(int *arr, int n)
{
  // whatever
}
*/

// GOOD
template <std::size_t size>
void betterFn(std
              : array<int, size> &arr)
{
    // whatever
}

void usingCArray()
{
    const int n = 256;
    // int arr[n] = {0};
    std::array<int, n> arr{};

    betterFn(arr);
    // fn(arr, n);
};
