// #13 (not using constexpr) doing work at runtime that could have been done at compile time

// int sumOf1ToN(const int n)
// Go ahead and le t the compiler know that its totally fine to compute this ahead of time
constexpr int sumOf1ToN(const int n)
{
  return n * (n + 1) / 2;
}

void usesSum()
{
  const int limit = 1000;
  auto triangleN = sumOf1ToN(limit);
  // use triangleN
}