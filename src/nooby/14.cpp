// #16 not knowing about default vs value initialization
#include <iostream>
#include <string>

void defaultVsValueInitialization()
{
  // these ones default initialized
  // they contain garbage until u put something into them
  int x;
  int *x2 = new int;

  // these ones are value initialized
  // they are guaranteed to contain the value zero
  // its a tiny bit less eficient, initialzing ur values is almost always a good idea
  int y{};
  int *y2 = new int{};
  int *y3 = new int{};

  // z, on the other hand is neither defautl or value initialized
  // is a function declaration
  int z();
}

struct S
{
  int n, m;
  std::string s;
};

// the same kind of thing goes if u have an aggregate or array type
void defaultVsValueInitialization2()
{
  // first two cases will be garbage and S will be the empty string
  S x;
  S *x2 = new S;

  // last three cases n and m are 0 and S is the empty string
  S y{};
  S *y2 = new S{};
  S *y3 = new S();
}