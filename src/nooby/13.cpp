// #15 thinking class members initialized in the order they appear in the initializer list
#include <iostream>

class View 
{
  public:
                                          // init list
    View(char *start, std::size_t size) : m_start{start}, m_end{start + size}
    {

    }

  private:
    char *m_end;
    char *m_start;
};