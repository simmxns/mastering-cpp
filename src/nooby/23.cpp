// #28 returning a shared pointer when u arent sure the object is going to be shared
#include <iostream>
#include <vector>
#include <string>

struct Pizza {};

std::shared_ptr<Pizza>
makeSharedPepperoniPizza(float diameter)
{
  std::vector<std::string> toppings = {"red sauce", "cheese", "pepperoni"};
  return std::make_shared<Pizza>(diameter, std::move(toppings)); // why shared?
}

void convertUniqueToSharedIsEasyAndCheap()
{
  // auto pizza = makeSharedPepperoniPizza(16.0f);
  // std::shared_ptr<Pizza> sharedPizza = std::move(pizza);
  
  // they could even directly assign the uniquew pointer return value to a shared pointer 
  // but if u return to them a shared pointer in the first place, the damage would already be done if all they really wanted was a unique pointer
  std::shared_ptr<Pizza> sharedPizza = makeSharedPepperoniPizza(16.0f);
}