// #6 any use of reinterpret cast

void anyUseOfReinterpretCast()
{
    long long x = 0;
    auto xp = reinterpret_cast<char *>(x);
    auto x2 = reinterpret_cast<long long>(xp);

    // reinterpreting the bytes of a float as a long is actually undefined
    // behavior
    float y = .123f;
    long i = *(long *)&y; // Sorry famous Quake III inv_sqrt code
    // ...
    y = *(float *)&i;
}

template <typename T>
void printBytes(const T &input)
{
    auto *bytes = reinterpret_cast<const std::byte *>(&input);

    // print out bytes at a time
}

// C++ 20
template <typename T>
void printBytes20(const T &input)
{
    using bytearray = std::array<std::byte, sizeof(T)>;
    // bit_cast interprets the bytes of one objects as a different type
    const auto &bytes = std::bit_cast<bytearray, T>(input);

    // print out bytes at a time
}
