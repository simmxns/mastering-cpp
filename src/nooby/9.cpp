// #11 not using structured bidings
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

void loopMapItems()
{
    std::unordered_map<std::string, std::string> colors = {
        {  "RED", "#ff0000"},
        {"GREEN", "#00ff00"},
        { "BLUE", "#0000ff"}
    };

    // Meh
    // for (const auto &pair: colors) {
    //   std::cout << "name: " << pair.first << ", hex: " << pair.second <<
    //   "\n";
    //   ...
    // Much Better
    for (const auto &[name, hex] : colors) {
        std::cout << "name: " << name << ", hex: " << hex << "\n";
    }
}

struct S {
    int a;
    std::string s;
};

S get_s();

void use_S() { const auto [name_for_a, name_for_s] = get_S(); }