// #30 confusing a const pointer with a pointer to const

void constPointerVsPointerToConst()
{
  int x = 0;
  int y = 0;

  // its the leftmost thing, in which it applies to the thing to its right
  // const applies to whatever is immediately to its left  

  // const applies to the int, not to the pointer
  const int *ptr1 = &x;
  // const applies to the int, not to the pointer
  int const *ptr2 = &x;
  // const applies to the pointer, not to the int
  int *const ptr3 = &x;
}