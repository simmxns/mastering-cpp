// #12 out-params instead of returning a struct

// Bad Idea
void getValuesOutParams(const int n, int &out1, int &out2)
{
    // do stuff
    out1 = n;
    out2 = n + 1;
}

// Better Idea
struct Values {
    int x, y;
};

Values getValuesReturnStruct(const int n) { return {n, n + 1}; }

void useValues() { auto [x, y] = getValuesReturnStruct(2); }
