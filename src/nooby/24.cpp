// #29 thinking that shared pointer in thread-safe
// the reference counting part of the shared pointer is thread-safe
#include <iostream>
#include <thread>

struct Resource
{
  int x{};
};

void worker(std::shared_ptr<Resource> noisy)
{
  for (int i = 0; i < 5000;  ++i)
    // the ref counting is thread-safe
    // this is a plain old data race of two threads trying to modify the same memory
    // if u want to fix the data race, u need to fix it the way u'd fix any other data race (with a mutex/lock/atomic/etc.)
    noisy->x++;
}

void sharedPtrIsNOTThreadsafe()
{
  auto r = std::make_shared<Resource>();
  std::thread t2(worker, r);
  std::thread t1(worker, r);
  r.reset();
}