// #18 attempting to add or remove elements from a container while looping over it
#include <iostream>
#include <vector>

void modifyWhileIterating()
{
  std::vector<int> v{1, 2, 3, 4};

  // adding or removing an element to the vector may invalidate the iterators to the vector
  // push_back() might need to resize the vector and move all the elements to a new location
  // after moving the contents of the vector to a new location, u cant expect the end pointer to be the same
  // for (auto x : v) {
  // this is a case where using a loop 
  // it doesnt matter if the contents of ur vector gets moved somewhere else the ith element is still the ith element
  const std::size_t size = v.size();
  for (std::size_t i = 0; i < size; ++i)
    v.push_back(v[i]);

  for (auto x: v) {
    std::cout << x;
  }

  std::cout << '\n';
}
