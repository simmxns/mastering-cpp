// #4 rewrite algorithms that already exists
#include <iostream>
#include <vector>

void knowYourAlgorithms()
{
    const std::vector<int> data = {-1, -3, -5, 8, 15, -1};

    // BAD
    /* std::size_t firstPosIdx;
    for (std::size_t i = 0; i < data.size(); ++i)  {
      if (data[i] > 0) {
        firstPosIdx = i;
        break;
      }
    } */

    const auto isPositive = [](const auto &x) { return x > 0; };
    auto firstPosIt = std::find_if(data.cbegin(), data.cend(), isPositive);

    // use firstPosIdx
}