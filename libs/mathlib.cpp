export module mathlib;

export auto add(int a, int b) -> int
{
	return a + b;
}

export auto subs(int a, int b) -> int
{
	return a - b;
}

export auto multiply(int a, int b) -> int
{
	return a * b;
}

export auto divide(int a, int b) -> int
{
	return a / b;
}
